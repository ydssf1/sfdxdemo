({
    doInit : function(component, event, helper) {
        console.log("carSearchResult controller doInit called");
        helper.onSearch(component,helper);
    },

    doCarTypeSearch : function(component, event, helper) {
        let params = event.getParam('arguments');
        if(params){
            component.set("v.carTypeId",params.cmpCarTypSearchId);
            helper.onSearch(component,helper);
        }
    },
    
    carSelected : function(component, event, helper) {
        console.log("car selecte event catched");
        component.set("v.selectedCarId",event.getParam("carId"));
        
    }
})