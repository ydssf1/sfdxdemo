({
	onSearch : function(component,helper) {
        console.log("carSearchResult controller helper called");
		let carSearch = component.get("c.carSearch");
        let varcarSearchId = component.get("v.carTypeId");
        
        carSearch.setParams({carSearchId : varcarSearchId });
        carSearch.setCallback(this,function(response){
            var state = response.getState();
            console.log('state value:'+state);
            if(state === 'SUCCESS'){
                component.set("v.cars", response.getReturnValue());
                component.set("v.carFound", true);
                console.log(component.get("v.cars"));
                console.log(component.get("v.carFound"));
            } else if(state === 'Error'){
                component.set("c.carFound", false);
                var errors = response.getError();
                if(errors){
                    console.log("Errors :"+errors);
                    if(errors[0] && errors[0].message){
                        throw new Error("Error"+errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(carSearch);
	}
})