({
    
    getCarTypeHelper : function(component){
        console.log('helper method called');
        let action = component.get("c.getCarTypes");
        action.setCallback(this,function(data){
            console.log('data state:'+data.getState());
            console.log('date value:'+data.getReturnValue()); 
            let state = data.getState();
            if(state === "SUCCESS"){
                component.set("v.carTypes",data.getReturnValue());
                console.log("carType value after set:"+component.get("v.carTypes"));
            }else if(state === "ERROR"){
                alert("unknow error occured");
            }
        });
        $A.enqueueAction(action);
    }
    
    //helper.callServer(component,"c.getCarTypes",function(response){component.set("v.carTypes",response)});

})