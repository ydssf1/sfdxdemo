({
    doInit : function(component,event,helper){
        let createCarSupported = $A.get("e.force:createRecord");
        if(createCarSupported){
            component.set("v.showNew",true);
        }else {
            component.set("v.showNew",false);
            console.log("create event not supported");
        }
        
        helper.getCarTypeHelper(component);
    },
    
    onSearchClick : function(component, event, helper) {
        var carSearchSubmit = component.getEvent("carSearch");
        var vcarSearchId = component.find("carTypeList").get("v.value");
        console.log('vcarSearchId:'+vcarSearchId)
        carSearchSubmit.setParams({
            carSearchId : vcarSearchId
        });
        carSearchSubmit.fire();
    }, 
        
    newValueSelected : function(component,event,helper){
        // let currentVal = component.find("carTypeList").get("v.value");
        // alert("Selected value is "+currentVal);
        let currentVal = event.getSource().get("v.value");
        if(currentVal){
            alert("Selected value is "+currentVal);   
        }  
    },
    
    onNewClick : function(component, event, helper){
        let createCarRecord = $A.get("e.force:createRecord");
        createCarRecord.setParams({
            "entityApiName" : "Car_Type__c"
        });
        createCarRecord.fire();
    }
})