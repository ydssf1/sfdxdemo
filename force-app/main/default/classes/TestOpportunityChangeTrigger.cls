@isTest
public class TestOpportunityChangeTrigger {

    @isTest
    public static void testCreateAndUpdateOpportunity(){
        Test.enableChangeDataCapture();
        Opportunity opp = new Opportunity();
        opp.Name = 'Sell 100 Widgets';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today().addMonths(3);
        insert opp;
        Test.getEventBus().deliver();
        Opportunity[] oppRecord = [SELECT Id, StageName FROM Opportunity];
        Opportunity opprec = oppRecord[0];
        opprec.StageName = 'Closed Won';
        update opprec;
        Test.getEventBus().deliver();
        Task[] tsk = [SELECT Subject FROM Task];
        System.debug('tsk size:'+tsk.size());
    }
}