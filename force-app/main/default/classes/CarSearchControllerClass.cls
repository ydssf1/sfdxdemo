public class CarSearchControllerClass {
	@auraEnabled
    public static List<Car__c> carSearch(String carSearchId){
        system.debug('carSearchId: '+carSearchId);
        List<Car__c> cars;
        if(carSearchId == Null || carSearchId.equalsIgnoreCase('')){
            System.debug('CarSearchControllerClass called if');
            cars = [SELECT Id, Contact__c, Geolocation__Latitude__s, Geolocation__Longitude__s, Mileage__c, Name, Picture__c 
                   FROM Car__c 
                  WHERE Available_for_Rent__c = true];
        } else if(carSearchId !=Null ){
            System.debug('CarSearchControllerClass called else if');
            cars = [SELECT Id, Contact__c, Geolocation__Latitude__s, Geolocation__Longitude__s, Mileage__c, Name, Picture__c 
                   FROM Car__c 
                  WHERE Available_for_Rent__c = true AND Car_Type__c =:carSearchId];
        }
        return cars;
    }
}