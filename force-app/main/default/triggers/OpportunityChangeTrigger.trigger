trigger OpportunityChangeTrigger on OpportunityChangeEvent (after insert) {
    List<Task> taskList = new List<Task>();
    
    for(OpportunityChangeEvent event : Trigger.new){
        EventBus.ChangeEventHeader header = event.ChangeEventHeader;
        System.debug('Received change event for ' + 
                     header.entityName +
                     ' for the ' + header.changeType + ' operation.'); 
        if (header.changetype == 'UPDATE') {
            if (event.IsWon == true) {
                taskList.add(new task(subject='Follow up on won opportunities: '+header.recordids, OwnerId = header.CommitUser));
            } 
        }
    }
    
    if(taskList.size()>0){
        insert taskList;
    }
}