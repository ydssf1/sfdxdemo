trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) {
        List<BatchLeadConvertErrors__c> bcelist = new List<BatchLeadConvertErrors__c>();
    for(BatchApexErrorEvent OBce : trigger.new){
        BatchLeadConvertErrors__c bc = new BatchLeadConvertErrors__c();
        bc.AsyncApexJobId__c = OBce.AsyncApexJobId;
        bc.Records__c = OBce.JobScope;
        bc.StackTrace__c = OBce.StackTrace;
        bcelist.add(bc);
    }
    insert bcelist;
}